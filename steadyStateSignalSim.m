function varargout = steadyStateSignalSim(varargin)
% STEADYSTATESIGNALSIM MATLAB code for steadyStateSignalSim.fig
%      STEADYSTATESIGNALSIM, by itself, creates a new STEADYSTATESIGNALSIM or raises the existing
%      singleton*.
%
%      H = STEADYSTATESIGNALSIM returns the handle to a new STEADYSTATESIGNALSIM or the handle to
%      the existing singleton*.
%
%      STEADYSTATESIGNALSIM('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in STEADYSTATESIGNALSIM.M with the given input arguments.
%
%      STEADYSTATESIGNALSIM('Property','Value',...) creates a new STEADYSTATESIGNALSIM or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before steadyStateSignalSim_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to steadyStateSignalSim_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help steadyStateSignalSim

% Last Modified by GUIDE v2.5 13-May-2015 11:44:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @steadyStateSignalSim_OpeningFcn, ...
                   'gui_OutputFcn',  @steadyStateSignalSim_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before steadyStateSignalSim is made visible.
function steadyStateSignalSim_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to steadyStateSignalSim (see VARARGIN)

% Choose default command line output for steadyStateSignalSim
handles.output = hObject;

handles.tissues = {'Kidney (Cortex)';
    'Kidney (Medulla)';
    'Liver';
    'Spleen';
    'Pancreas';
    'Paravertebral Muscle';
    'Bone Marrow (L4)';
    'Uterus (Myometrium)';
    'Uterus (Endometrium)';
    'Uterus (Cervix)';
    'Prostate'};

% (tissue, field strength)
handles.T1 = [966	1142;
    1412	1545;
    586	809;
    1057	1328;
    584	725;
    856	898;
    549	586;
    343	382;
    1309	1514;
    1274	1453;
    1135	1616;
    1317	1597];

% (tissue, field strength)
handles.T2 = [87	76;
    85	81;
    46	34;
    79	61;
    46	43;
    27	29;
    49	49;
    58	68;
    117	79;
    101	59;
    58	83;
    88	74];

set(handles.listbox_tissue1,'String',handles.tissues);
set(handles.listbox_tissue2,'String',handles.tissues);

handles.tissue1 = 1;
handles.tissue2 = 1;
handles.field = 1;
handles.sequence = 1;
handles.te = 2;
handles.tr = 4;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes steadyStateSignalSim wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = steadyStateSignalSim_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in listbox_tissue1.
function listbox_tissue1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox_tissue1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox_tissue1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox_tissue1

handles.tissue1 = get(hObject,'Value');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function listbox_tissue1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox_tissue1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox_tissue2.
function listbox_tissue2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox_tissue2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox_tissue2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox_tissue2

handles.tissue2 = get(hObject,'Value');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function listbox_tissue2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox_tissue2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox_sequence.
function listbox_sequence_Callback(hObject, eventdata, handles)
% hObject    handle to listbox_sequence (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox_sequence contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox_sequence

handles.sequence = get(hObject,'Value');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function listbox_sequence_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox_sequence (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_te_Callback(hObject, eventdata, handles)
% hObject    handle to edit_te (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_te as text
%        str2double(get(hObject,'String')) returns contents of edit_te as a double

handles.te = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_te_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_te (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_tr_Callback(hObject, eventdata, handles)
% hObject    handle to edit_tr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_tr as text
%        str2double(get(hObject,'String')) returns contents of edit_tr as a double

handles.tr = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_tr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_tr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_simulate.
function pushbutton_simulate_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_simulate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

TE = handles.te;
TR = handles.tr;
field = handles.field;
tiss1 = handles.tissue1;
tiss2 = handles.tissue2;
flip = 1:90;

% tissue 1 signal
T1 = handles.T1(tiss1,field);
T2 = handles.T2(tiss1,field);
switch handles.sequence
    % SPGR
    case 1
        S1 = sind(flip).*(1 - exp(-TR/T1))./(1 - cosd(flip).*exp(-TR/T1)).*exp(-TE/T2);
    % bSSFP
    case 2
        S1 = sind(flip)./((T1/T2)*(1 - cosd(flip)) + (1 + cosd(flip))).*exp(-TE/T2);
end

% tissue 2 signal
T1 = handles.T1(tiss2,field);
T2 = handles.T2(tiss2,field);
switch handles.sequence
    % SPGR
    case 1
        S2 = sind(flip).*(1 - exp(-TR/T1))./(1 - cosd(flip).*exp(-TR/T1)).*exp(-TE/T2);
    % bSSFP
    case 2
        S2 = sind(flip)./((T1/T2)*(1 - cosd(flip)) + (1 + cosd(flip))).*exp(-TE/T2);
end

dS = abs(S2 - S1);


axes(handles.axes1); cla; axes(handles.axes1);
plot(flip,S1,'linewidth',2); hold on;
plot(flip,S2,'-r','linewidth',2); hold on;
plot(flip,dS,'-g','linewidth',2); hold on;
legend({handles.tissues{tiss1}; handles.tissues{tiss2}; ['|',handles.tissues{tiss2},'-',handles.tissues{tiss1},'|']});
seq = get(handles.listbox_sequence,'String');
title(seq{handles.sequence}); grid on;
xlabel('Flip Angle [deg]');
ylabel('Signal [A.U.]');

SS = [S1, S2];
% [~,pinds] = findpeaks(dS);
[~,pinds] = max(dS);
if ~isempty(pinds)
    for n = 1:length(pinds)
        optFlip = flip(pinds(n));
        xx = [optFlip, optFlip];
        yy = [0, max(SS(:))];
        hold on; plot(xx,yy,'--g');
    end
end

% [~,pinds] = findpeaks(S1);
[~,pinds] = max(S1);
if ~isempty(pinds)
    optFlip = flip(pinds(1));
    xx = [optFlip, optFlip];
    yy = [0, max(SS(:))];
    hold on; plot(xx,yy,'--b');
end

% [~,pinds] = findpeaks(S2);
[~,pinds] = max(S2);
if ~isempty(pinds)
    optFlip = flip(pinds(1));
    xx = [optFlip, optFlip];
    yy = [0, max(SS(:))];
    hold on; plot(xx,yy,'--r');
end

% --- Executes on selection change in listbox_fieldstrength.
function listbox_fieldstrength_Callback(hObject, eventdata, handles)
% hObject    handle to listbox_fieldstrength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox_fieldstrength contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox_fieldstrength

handles.field = get(hObject,'Value');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function listbox_fieldstrength_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox_fieldstrength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
